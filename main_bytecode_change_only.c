/*********************************************************
	duCx interpreter Version 0.000005
	Copyright 2021 Gregg Ink
	Zlib license

	To compile:
	gcc -c technical.c
	gcc -o ducx main_bytecode_change_only.c technical.o -lpthread;

**********************************************************/
#include "stdio.h"
#include "technical.h"
#include "tc.h"

#define DUC_MAX_STRING_SIZE 4095
#define DUC_MAX_STRING_SIZE_INC 4096

enum tok_type{DUC_TOK_ERROR, DUC_TOK_FUNC_NAME, DUC_TOK_DATATYPE, DUC_TOK_VAR_NAME, DUC_TOK_STRING, DUC_TOK_NUMBER, DUC_TOK_KEYWORD, DUC_TOK_EOF};
enum op_type{DUC_OP_FUNC_0, DUC_OP_FUNC_3, DUC_OP_FUNC_10, DUC_OP_INT_ADD};
enum var_type{DUC_VAR_VOID, DUC_VAR_INT};

typedef struct _ducx ducx;
typedef struct _loc loc;
typedef struct _loc_long loc_long;
typedef struct _token token;
typedef struct _var var;

struct _var{

	int type;
//	char name[32];	//max 31 character names and NULL character
	union {
		uint64_t ui64;
		int i;
		float f;
		char *str;
	} value;
	int len;

};

//loc: line of code
struct _loc{

	uint32_t id;
	uint32_t type;
	uint64_t operator;
	var *operand1;
	var *operand2;
	var *operand3;
	var ret;	//return value of the loc

};

struct _loc_long{

	uint32_t id;
	uint32_t type;
	uint64_t operator;
	var *operand[11];
	var ret;	//return value of the loc

};

struct _token{

	int type;
	char *start;
	int len;
	char *str;
	int str_len;

	char *start_next;	//start of next token

};

var v_list[127];

struct _ducx{

	//used to keep track of parsing of bytecode
	int ii;	//instruction index

	//bytecode
	loc block[128];

};

//////////////////////////////////
// prototypes specific to DuCx

#define duc_print_error(MSG) fprintf(stderr, "\n%sDUCX ERROR: %s%s\n", TC_YEL, TC_NRM, MSG)

//parsing functions
void duc_parse_code(char *buffer, ducx *d);
void duc_parse_function(ducx *d, token *tok);
void duc_parse_print_string(ducx *d, token *tok);
void duc_parse_print_int(ducx *d, token *tok);
void duc_parse_print_error(ducx *d, token *tok);

//bytecode functions
void duc_run_bytecode(ducx *d);
void duc_exec_print_string(var *v, var *ret);
void duc_exec_print_int(var *v, var *ret);
void duc_exec_print_error(var *v, var *ret);

token* duc_get_next_token(token *tok);
char* duc_skip_shebang(char *buffer);
char* duc_string_unescape_double_quotes(char *str);
int duc_check_datatype(token *tok);
void duc_create_variable(token *tok);
var* duc_get_var(token *tok);

void duc_debug_display_token(token *tok);

/////////////////////////////////

int main(int argc, char **argv){

	if(argc < 2){
		duc_print_error("please provide a valid duCx sourcefile\n");
		return 1;
	}

	if(!tec_file_exists(argv[1])){
		duc_print_error("file does not appear to exists or cannot be accessed\n");
		return 3;
	}

	char *buffer = tec_file_get_contents(argv[1]);

	if(!buffer){
		duc_print_error("failed to load the file");
	}

	//allocate main ducx struct
	ducx *d = (ducx *) malloc(sizeof(ducx));
	memset(d, 0, sizeof(ducx));

	buffer = duc_skip_shebang(buffer);

	duc_parse_code(buffer, d);
	duc_run_bytecode(d);

	return 0;

}//main*/

void duc_parse_code(char *buffer, ducx *d){

	token tok;
	memset(&tok, 0, sizeof(tok));
	tok.start_next = buffer;
	tok.str = (char *) malloc(sizeof(char) * DUC_MAX_STRING_SIZE_INC);
	tok.str[0] = 0;

	while(tok.type != DUC_TOK_EOF){
		duc_get_next_token(&tok);

		switch(tok.type){
		case DUC_TOK_FUNC_NAME:
			duc_parse_function(d, &tok);
			break;
		case DUC_TOK_DATATYPE:
			duc_create_variable(&tok);
			break;
		}
	}

}//duc_parse_bytecode*/

void duc_parse_function(ducx *d, token *tok){

	if( tec_buf_begins(tok->start, "print_string") ){
		duc_parse_print_string(d, tok);
		return;
	}

	if( tec_buf_begins(tok->start, "print_error") ){
		duc_parse_print_error(d, tok);
		return;
	}

	if( tec_buf_begins(tok->start, "print_int") ){
		duc_parse_print_int(d, tok);
		return;
	}

	duc_print_error("Unknown function");
	exit(1);

}//duc_parse_function*/

void duc_parse_print_string(ducx *d, token *tok){

	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_STRING && tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("print_string() should get a string (for now)");
		exit(1);
	}

	loc *l = &(d->block[d->ii]);
	l->id = 1;
	l->type = DUC_OP_FUNC_3;
	l->operator = &duc_exec_print_string;

	if(tok->type == DUC_TOK_VAR_NAME ){
		var *v = duc_get_var(tok);
		l->operand1 = v;
	}

	if(tok->type == DUC_TOK_STRING ){
		l->ret.value.str = tok->start;
		l->ret.len = tok->len;
	}

	d->ii += 1;

	//TODO(GI):
	//for now, we assume print_string ends as it should
	tok->start_next += 2;	//skip over );

}//duc_parse_print_string*/

void duc_parse_print_int(ducx *d, token *tok){

	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_NUMBER && tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("print_int() should get an int");
		exit(1);
	}

	loc *l = &(d->block[d->ii]);
	l->id = 1;
	l->type = DUC_OP_FUNC_3;
	l->operator = &duc_exec_print_int;

	if(tok->type == DUC_TOK_VAR_NAME ){
		//TODO(GI):
		// check we have an int and not something else
		var *v = duc_get_var(tok);
		l->operand1 = v;
	}

	if(tok->type == DUC_TOK_NUMBER ){
		l->ret.value.str = tok->start;
		l->ret.len = tok->len;
	}

	d->ii += 1;

	//TODO(GI):
	//for now, we assume print_int ends as it should
	tok->start_next += 2;	//skip over );

	return;

}//duc_parse_print_int*/

void duc_parse_print_error(ducx *d, token *tok){

	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_STRING && tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("print_error() takes a string");
		exit(1);
	}

	loc *l = &(d->block[d->ii]);
	l->id = 1;
	l->type = DUC_OP_FUNC_3;
	l->operator = &duc_exec_print_error;

	if(tok->type == DUC_TOK_VAR_NAME ){
		var *v = duc_get_var(tok);
		l->operand1 = v;
	}

	if(tok->type == DUC_TOK_STRING ){
		l->ret.value.str = tok->start;
		l->ret.len = tok->len;
	}

	d->ii += 1;

	//TODO(GI):
	//for now, we assume print_string ends as it should
	tok->start_next += 2;	//skip over );

}//duc_parse_print_error*/

void duc_run_bytecode(ducx *d){

	void (*func_0)(var *ret);
	void (*func_v)(var *v, var *ret);

	loc *l = &(d->block[0]);

	while(l->type){	//TODO(GI): make this more elegant

		switch(l->type){
		case DUC_OP_FUNC_0:
			//this function takes zero arguments
			//not including space to store return value
			func_0 = l->operator;
			func_0( &(l->ret) );
			break;
		case DUC_OP_FUNC_3:
			func_v = l->operator;
			func_v( l->operand1, &(l->ret) );
			break;
		case DUC_OP_FUNC_10:
			break;
		case DUC_OP_INT_ADD:
			if(l->operand2){
				l->ret.value.i = l->operand1->value.i + l->operand2->value.i;
			}else{
				//if we add with a constant, we store it in the return value
				l->ret.value.i = l->operand1->value.i + l->ret.value.i;
			}
			break;
		}

		//TODO(GI):
		//more complex logic will be needed here
		//once we use loc_long
		l += 1;
	}

}//duc_run_bytecode*/

void duc_exec_print_string(var *v, var *ret){

	if(v){
		write(1, v->value.str, v->len);
	}else{
		write(1, ret->value.str, ret->len);
	}

	ret->type = DUC_VAR_INT;
	ret->value.i = 1;

}//duc_exec_print_string*/

void duc_exec_print_int(var *v, var *ret){

	if(v){
		char tmp[13];
		int n = tec_string_from_int( v->value.i, tmp, 12);
		tmp[n] = 10;
		n += 1;
		tmp[n] = 0;
		write(1, tmp, n);
	}else{
		write(1, ret->value.str, ret->len);
		putchar(10);
	}

	ret->type = DUC_VAR_INT;
	ret->value.i = 1;

}//duc_exec_print_int*/

void duc_exec_print_error(var *v, var *ret){
//TODO(GI):
//clean this up a bit

	char tmp[7] = "x[1;31m";
	tmp[0] = 27;
	write(2, tmp, 7);

	if(v){
		write(2, v->value.str, v->len);
	}else{
		write(2, ret->value.str, ret->len);
	}

	char tmp2[5] = "x[0m";
	tmp2[0] = 27;
	tmp2[4] = 10;//new line
	write(2, tmp2, 5);

	ret->type = DUC_VAR_INT;
	ret->value.i = 1;

}//duc_exec_print_error*/

token* duc_get_next_token(token *tok){

	char *buffer = tok->start_next;
	int len = 0;

	tok->str[0] = 0;
	tok->str_len = 0;

	while(*buffer && tec_char_is_white_space(*buffer)){
		buffer += 1;
	}

	if( !(*buffer) ){
		tok->type = DUC_TOK_EOF;
		return tok;
	}

	tok->start = buffer;
	if(*buffer == '\"'){
		// tokenizer finds a string
		char prev = *buffer;
		buffer += 1;
		len += 1;
		while(*buffer && *buffer != '\"'){
			prev = *buffer;
			buffer += 1;
			len += 1;
			if(*buffer && *buffer == '\"' && prev == '\\'){
				buffer += 1;
				len += 1;
			}
		}

		//sanity checks for the strings
		if(!(*buffer)){
			duc_print_error("Unexpected end of file");
			exit(1);
		}
		if(len > DUC_MAX_STRING_SIZE){
			duc_print_error("String is too long!");
			exit(1);
		}

		*buffer = 0;
//		tec_string_copy(tok->str, tok->start+1, len);
		tok->start += 1;
		duc_string_unescape_double_quotes(tok->start);
		buffer += 1;
//		len += 1;
		tok->len = tec_string_length(tok->start);

		//consider string not ending!!
//		tok->len = len;
		tok->type = DUC_TOK_STRING;
		tok->start_next = buffer;
		return tok;

	}

	if(*buffer == '$'){
		// tokenizer finds a variable name
		buffer += 1;
		len += 1;
		while(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') || (*buffer >= '0' && *buffer <= '9') || (*buffer == '_') ) ){
			//NOTE(GI):
			//our var can contain numerals 0 -9, just not begin with one
			len += 1;
			buffer += 1;
		}
		tok->len = len;
		tok->type = DUC_TOK_VAR_NAME;
		tok->start_next = buffer;
		return tok;
	}

	if( *buffer >= '0' && *buffer <= '9' ){
		// tokenizer finds a number
		buffer += 1;
		len += 1;
		while( *buffer && *buffer >= '0' && *buffer <= '9' ){
			buffer += 1;
			len += 1;
		}

		tok->len = len;
		tok->type = DUC_TOK_NUMBER;
		tok->start_next = buffer;
		return tok;
	}

	if(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') ) ){
		// tokenizer finds either keyword or function name

		while(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') || (*buffer >= '0' && *buffer <= '9') || (*buffer == '_') ) ){
			//NOTE(GI):
			//our function name can contain numerals 0 -9 and underscore, just not begin with one
			len += 1;
			buffer += 1;
		}

		tok->len = len;

		while( tec_char_is_white_space(*buffer) ){
			buffer += 1;
		}

		// TODO(GI):
		// assumption that '(' always follows function name
		// will not be valid for long
		if( *buffer == '(' ){
			tok->type = DUC_TOK_FUNC_NAME;
			tok->start_next = buffer + 1;
			return tok;
		}

		if( duc_check_datatype(tok) ){
			tok->type = DUC_TOK_DATATYPE;
			return tok;
		}

		tok->type = DUC_TOK_KEYWORD;
		tok->start_next = buffer;
		return tok;

	}

	return NULL;

}//duc_get_next_token*/

char* duc_skip_shebang(char *buffer){

	if(buffer[0] == '#' && buffer[1] == '!'){
		buffer += 2;
		while(*buffer && *buffer != '\n'){
			buffer += 1;
		}
		if(!*buffer){
			duc_print_error("Did not find new line at end of shebang line\n");
			exit(1);
		}
		buffer += 1;
	}

	return buffer;

}//duc_skip_shebang*/

char* duc_string_unescape_double_quotes(char *str){

	if(!str)
		return str;

	char *original = str;
	while(*str){
		if(*str == '\\' && str[1] == '\"'){
			tec_string_shift(str);
		}
		str += 1;
	}

	return original;

}//duc_string_unescape_double_quotes*/

int duc_check_datatype(token *tok){

	if(!tok)
		return 0;

	if( tec_buf_begins(tok->start, "int") && tok->len == 3 ){
		return 1;
	}
	if( tec_buf_begins(tok->start, "char") && tok->len == 4 ){
		duc_print_error("char not currently supported\n");
		exit(1);
	}
	if( tec_buf_begins(tok->start, "float") && tok->len == 5 ){
		duc_print_error("float not currently supported\n");
		exit(1);
	}

	return 0;

}//duc_check_datatype*/

void duc_create_variable(token *tok){

	int pointer = 0;
	char *str = tok->start + tok->len;

	while(*str && tec_char_is_white_space(*str)){	//redundant, does tokenizer not skip white space?
		str += 1;
	}

	tok->start_next = str;
	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("Invalid declaration of variable");
		exit(1);
	}

//	duc_debug_display_token(tok);
	int hash = (tok->start[1] + tok->start[tok->len-1])%127;

	if( (v_list[hash]).type ){
		hash += 1;
		while(hash >= 127){
			//TODO(GI): prevent infinite loop when array is full
			hash = 0;
		}
	}

	var *v = &(v_list[hash]);
	if( tec_buf_begins(str, "int") ){
		v->type = DUC_VAR_INT;
	}
//	tec_string_copy(v->name, tok->start, tok->len + 2);

	//check for initialization
	char *eq = tok->start + tok->len;
	while(*eq && tec_char_is_white_space(*eq)){
		eq += 1;
	}
	if(*eq != '='){
		tok->start_next = tok->start + tok->len + 3;//skip over newline
		//TODO(GI): won't work when semi-colon does not follow straight away
		return;
	}

	tok->start_next = eq + 1;
	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_NUMBER){
		duc_print_error("Integers can only be filled in with numbers");
		exit(1);
	}

	v->value.i = tec_string_to_int(tok->start);
	tok->start_next = tok->start + tok->len + 1;

}//duc_create_variable*/

var* duc_get_var(token *tok){

	int hash = (tok->start[1] + tok->start[tok->len-1])%127;

	return &(v_list[hash]);

}//duc_get_var*/

void duc_debug_display_token(token *tok){

	if(!tok)
		return;

	char *tmp = tok->start;
	int len = tok->len;
	printf("token is *%s", TC_RED);
	while(len){
		putchar(*tmp);
		len -= 1;
		tmp += 1;
	}
	printf("%s*\n", TC_NRM);

}//duc_debug_display_token*/
