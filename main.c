/*********************************************************
	duCx interpreter Version 0.000006
	Copyright 2021 Gregg Ink
	Zlib license

	To compile:
	gcc -c technical.c
	gcc -o ducx main.c technical.o -lpthread;

**********************************************************/
#include "stdio.h"
#include "technical.h"
#include "tc.h"

#define DUC_MAX_STRING_SIZE 4095
#define DUC_MAX_STRING_SIZE_INC 4096
#define DUC_MAX_NUMBER_VARS 127

//TODO(GI):
//allow for arbitrary long programs
//with many more loc than 1024
#define DUC_LOC_IN_BLOCK 1024

enum tok_type{DUC_TOK_ERROR, DUC_TOK_FUNC_NAME, DUC_TOK_DATATYPE, DUC_TOK_VAR_NAME, DUC_TOK_STRING, DUC_TOK_NUMBER, DUC_TOK_KEYWORD, DUC_TOK_EQUAL, DUC_TOK_MATH_OP, DUC_TOK_CLOSING_BLOCK, DUC_TOK_DELIMETER, DUC_TOK_OPEN_BRACKET, DUC_TOK_CLOSED_BRACKET, DUC_TOK_EOF};

//different types of operations
//naming conventions
//some ops end in _VV _VC _CV
//variable with variable (_VV)
//variable with constant (_VC)
//constant with variable (_CV)
//L refers to the returns value of another line of code (LOC)
//etc
enum op_type{DUC_OP_NOP, DUC_OP_INT_ASSIGN_L, DUC_OP_INT_ASSIGN_V, DUC_OP_INT_ASSIGN_C, DUC_OP_FUNC_0, DUC_OP_FUNC_3, DUC_OP_FUNC_11, DUC_OP_WHILE, DUC_OP_JUMP, DUC_OP_INT_ADD_LL, DUC_OP_INT_ADD_LV, DUC_OP_INT_ADD_LC, DUC_OP_INT_ADD_VL, DUC_OP_INT_ADD_VV, DUC_OP_INT_ADD_VC, DUC_OP_INT_ADD_CL, DUC_OP_INT_ADD_CV, DUC_OP_INT_ADD_CC, DUC_OP_INT_SUB_LL, DUC_OP_INT_SUB_LV, DUC_OP_INT_SUB_LC, DUC_OP_INT_SUB_VL, DUC_OP_INT_SUB_VV, DUC_OP_INT_SUB_VC, DUC_OP_INT_SUB_CL, DUC_OP_INT_SUB_CV, DUC_OP_INT_SUB_CC, DUC_OP_INT_MUL_LL, DUC_OP_INT_MUL_LV, DUC_OP_INT_MUL_LC, DUC_OP_INT_MUL_VL, DUC_OP_INT_MUL_VV, DUC_OP_INT_MUL_VC, DUC_OP_INT_MUL_CL, DUC_OP_INT_MUL_CV, DUC_OP_INT_MUL_CC, DUC_OP_INT_DIV_LL, DUC_OP_INT_DIV_LV, DUC_OP_INT_DIV_LC, DUC_OP_INT_DIV_VL, DUC_OP_INT_DIV_VV, DUC_OP_INT_DIV_VC, DUC_OP_INT_DIV_CL, DUC_OP_INT_DIV_CV, DUC_OP_INT_DIV_CC};
enum var_type{DUC_VAR_VOID, DUC_VAR_INT};

typedef struct _ducx ducx;
typedef struct _loc loc;
typedef struct _loc_long loc_long;
typedef struct _token token;
typedef struct _var var;
typedef struct _term term;

struct _term{

	var *v;
	char op;	//operation
	loc *ret;
	union{
		int64_t i64;
		int32_t i;
		float f;
		double d;
	} value;

};

struct _var{

	int type;
//	char name[32];	//max 31 character names and NULL character
	union {
		int64_t i64;
		int i;
		float f;
		char *str;
	} value;
	int len;

};

//loc: line of code
struct _loc{

	uint32_t flags;
	uint32_t type;
	void *operator;
	var *operand1;
	var *operand2;
	var *operand3;
	var ret;	//return value of the loc

};

struct _loc_long{

	uint32_t flags;
	uint32_t type;
	uint64_t operator;
	var *operand[11];
	var ret;	//return value of the loc

};

struct _token{

	int type;
	char *start;
	int len;

	char *start_next;	//start of next token

};

struct _ducx{

	//used to keep track of parsing of bytecode
	int ii;	//instruction index

	//bytecode
	loc block[DUC_LOC_IN_BLOCK];

	//keep track of nested control structures
	//control structure pointers
	loc *csp[127];
	int csp_counter;

	//list of variables
	var v_list[DUC_MAX_NUMBER_VARS];
	char v_list_names[DUC_MAX_NUMBER_VARS][32];

};

//////////////////////////////////
// prototypes of DuCx

#define duc_print_error(MSG) fprintf(stderr, "\n%sDUCX ERROR: %s%s\n", TC_YEL, TC_NRM, MSG)

char* duc_skip_shebang(char *buffer);

//parsing functions
void duc_parse_code(char *buffer, ducx *d);
loc* duc_parse_next_loc(ducx *d);
void duc_parse_function(ducx *d, token *tok);
void duc_parse_statement(ducx *d, token *tok);
loc* duc_parse_expression(ducx *d, token *tok, int level);
void duc_parse_while(ducx *d, token *tok);
void duc_parse_print_string(ducx *d, token *tok);
void duc_parse_print_int(ducx *d, token *tok);
void duc_parse_exit(ducx *d, token *tok);
void duc_parse_print_error(ducx *d, token *tok);

//bytecode functions
void duc_run_bytecode(ducx *d);
void duc_exec_print_string(var **w, var *ret);
void duc_exec_print_int(var **w, var *ret);
void duc_exec_exit(var **w, var *ret);
void duc_exec_print_error(var **w, var *ret);

token* duc_get_next_token(token *tok);
char* duc_string_unescape_double_quotes_and_others(char *str);
int duc_check_datatype(token *tok);
int duc_check_keyword(token *tok);
void duc_create_variable(ducx *d, token *tok);
var* duc_get_var(ducx *d, token *tok);

void duc_debug_display_token(token *tok);
void duc_debug_print_block(ducx *d);
void duc_debug_display_terms(term *t);

/////////////////////////////////

int main(int argc, char **argv){

	if(argc < 2){
		duc_print_error("please provide a valid duCx sourcefile\n");
		return 1;
	}

	if(!tec_file_exists(argv[1])){
		duc_print_error("file does not appear to exists or cannot be accessed\n");
		return 3;
	}

	char *buffer = tec_file_get_contents(argv[1]);

	if(!buffer){
		duc_print_error("failed to load the file\n");
	}

	//allocate main ducx struct
	ducx *d = (ducx *) malloc(sizeof(ducx));
	memset(d, 0, sizeof(ducx));

	buffer = duc_skip_shebang(buffer);

	duc_parse_code(buffer, d);
	duc_run_bytecode(d);

	return 0;

}//main*/

char* duc_skip_shebang(char *buffer){

	if(buffer[0] == '#' && buffer[1] == '!'){
		buffer += 2;
		while(*buffer && *buffer != '\n'){
			buffer += 1;
		}
		if(!*buffer){
			duc_print_error("Did not find new line at end of shebang line\n");
			exit(1);
		}
		buffer += 1;
	}

	return buffer;

}//duc_skip_shebang*/

void duc_parse_code(char *buffer, ducx *d){

	token tok;
	memset(&tok, 0, sizeof(tok));
	tok.start_next = buffer;
	loc * l = NULL;

	while(tok.type != DUC_TOK_EOF){
		duc_get_next_token(&tok);
		switch(tok.type){
		case DUC_TOK_FUNC_NAME:
			duc_parse_function(d, &tok);
			break;
		case DUC_TOK_KEYWORD:
			//////!!!!!
			duc_parse_function(d, &tok);
			break;
		case DUC_TOK_DATATYPE:
			duc_create_variable(d, &tok);
			break;
		case DUC_TOK_VAR_NAME:
			duc_parse_statement(d, &tok);
			break;
		case DUC_TOK_CLOSING_BLOCK:
			l = duc_parse_next_loc(d);
			l->type = DUC_OP_JUMP;
			if(!d->csp_counter){
				duc_print_error("Incorrect nesting of control structures\n");
				exit(1);
			}
			d->csp_counter -= 1;
			loc* l_to = d->csp[d->csp_counter];
			d->csp[d->csp_counter] = NULL;
			l->operand1 = l_to;
			l_to->operand2 = l + 1;
			break;
		}
	}

}//duc_parse_bytecode*/

loc* duc_parse_next_loc(ducx *d){

	loc *l = &(d->block[d->ii]);
	d->ii += 1;
	if(d->ii > DUC_LOC_IN_BLOCK){
		duc_print_error("block full\n");
		exit(1);
	}

	return l;

}//duc_parse_next_loc*/

void duc_parse_function(ducx *d, token *tok){

	if( tec_buf_begins(tok->start, "while") ){
		duc_parse_while(d, tok);
		return;
	}

	if( tec_buf_begins(tok->start, "print_string") ){
		duc_parse_print_string(d, tok);
		return;
	}

	if( tec_buf_begins(tok->start, "print_error") ){
		duc_parse_print_error(d, tok);
		return;
	}

	if( tec_buf_begins(tok->start, "print_int") ){
		duc_parse_print_int(d, tok);
		return;
	}

	if( tec_buf_begins(tok->start, "exit") ){
		duc_parse_exit(d, tok);
		return;
	}

	duc_print_error("Unknown function\n");
	exit(1);

}//duc_parse_function*/

void duc_parse_statement(ducx *d, token *tok){

	var *v = duc_get_var(d, tok);

	duc_get_next_token(tok);

	if(tok->type == DUC_TOK_EQUAL){
		loc* l = duc_parse_expression(d, tok, 0);
		if(l){
			l->operand1 = v;
		}
	}else{
		duc_print_error("Unrecognized statement");
		exit(1);
	}

}//duc_parse_statement*/

loc* duc_parse_expression(ducx *d, token *tok, int level){

	if(level > 63){
		duc_print_error("too many nested brackets\n");
		exit(1);
	}

	//TODO(GI):
	//later allow for function calls within expressions

	//NOTE(GI):
	//maximum 63 terms in an expression plus NULL at end
	term t[64];
	memset(t, 0, sizeof(t));
	int ti = 0;	//term index
	int ti_next = 0;
	loc *l = NULL;

	while(tok->type != DUC_TOK_DELIMETER && tok->type != DUC_TOK_CLOSED_BRACKET){
		duc_get_next_token(tok);
		switch(tok->type){
		case DUC_TOK_OPEN_BRACKET:
			l = duc_parse_expression(d, tok, level + 1);
			t[ti].ret = l;
			duc_get_next_token(tok);
			switch(tok->type){
			case DUC_TOK_MATH_OP:
				t[ti].op = tok->start[0];
				break;
			case DUC_TOK_DELIMETER:
			case DUC_TOK_CLOSED_BRACKET:
				//do nothing
				break;
			default:
				duc_print_error("unexpected token in expression (4)\n");
				exit(1);
				break;
			}
			ti += 1;
			break;
		case DUC_TOK_VAR_NAME:
			t[ti].v = duc_get_var(d, tok);
			duc_get_next_token(tok);
			switch(tok->type){
			case DUC_TOK_MATH_OP:
				t[ti].op = tok->start[0];
				break;
			case DUC_TOK_DELIMETER:
			case DUC_TOK_CLOSED_BRACKET:
				//do nothing
				break;
			default:
				duc_print_error("unexpected token in expression (1)\n");
				exit(1);
				break;
			}
			ti += 1;
			break;
		case DUC_TOK_NUMBER:
			t[ti].value.i = tec_string_to_int(tok->start);
			duc_get_next_token(tok);
			switch(tok->type){
			case DUC_TOK_MATH_OP:
				t[ti].op = tok->start[0];
				break;
			case DUC_TOK_DELIMETER:
			case DUC_TOK_CLOSED_BRACKET:
				//do nothing
				break;
			default:
				duc_print_error("unexpected token in expression (2)\n");
				exit(1);
				break;
			}
			ti += 1;
			break;
		default:
			duc_print_error("unexpected token in expression (3)\n");
			exit(1);
			break;
		}
	}

	if(!ti){
		duc_print_error("Empty or badly formed expression\n");
		exit(1);
	}

	if(ti == 1){
		l = duc_parse_next_loc(d);
		l->type = DUC_OP_INT_ASSIGN_V;
		if(t[0].ret){
			l->operand2 = t[0].ret;
			l->type -= 1;
		}else{
			if(t[0].v){
				l->operand2 = t[0].v;
			}else{
				l->operand2 = t[0].value.i;
				l->type += 1;
			}
		}
		return l;
	}

	ti = 0;
	while(t[ti].op){//null when at end of terms
		if(t[ti].op == '*' || t[ti].op == '/'){
			ti_next = ti + 1;
			while(t[ti_next].op == ' '){
				ti_next += 1;
			}
			//produce bytecode here
			l = duc_parse_next_loc(d);
			if(t[ti].op == '*'){
				l->type = DUC_OP_INT_MUL_VV;
			}else{
				l->type = DUC_OP_INT_DIV_VV;
			}
			//TODO(GI):
			//consider *_CC case where we execute 
			//operation right here
			//TODO(GI):
			//remove locs with identity elements
			//i.e. + 0 and * 1
			if(t[ti].ret){
				l->operand2 = t[ti].ret;
				l->type -= 3;
			}else{
				if(t[ti].v){
					l->operand2 = t[ti].v;
				}else{
					l->operand2 = t[ti].value.i;
					l->type += 3;	//sets it from *_VV to *_CV
				}
			}
			if(t[ti_next].ret){
				l->operand3 = t[ti_next].ret;
				l->type -= 1;	// sets it from *_LV to *_LL
			}else{
				if(t[ti_next].v){
					l->operand3 = t[ti_next].v;
				}else{
					l->operand3 = t[ti_next].value.i;
					l->type += 1;	//sets it from *_VV to *_VC
				}
			}
			t[ti_next].ret = l;
			t[ti].op = ' ';
		}
		ti += 1;
	}

	ti = 0;
	while(t[ti].op){

		if(t[ti].op == '+' || t[ti].op == '-'){
			ti_next = ti + 1;
			while(t[ti_next].op == ' '){
				ti_next += 1;
			}
			//produce bytecode here
			l = duc_parse_next_loc(d);
			if(t[ti].op == '+'){
				l->type = DUC_OP_INT_ADD_VV;
			}else{
				l->type = DUC_OP_INT_SUB_VV;
			}
			if(t[ti].ret){
				l->operand2 = t[ti].ret;
				l->type -= 3;
			}else{
				if(t[ti].v){
					l->operand2 = t[ti].v;
				}else{
					l->operand2 = t[ti].value.i;
					l->type += 3;	//sets it from *_VV to *_CV
				}
			}
			if(t[ti_next].ret){
				l->operand3 = t[ti_next].ret;
				l->type -= 1;	// sets it from *_LV to *_LL
			}else{
				if(t[ti_next].v){
					l->operand3 = t[ti_next].v;
				}else{
					l->operand3 = t[ti_next].value.i;
					l->type += 1;	//sets it from *_VV to *_VC
				}
			}
			t[ti_next].ret = l;
			t[ti].op = ' ';//redandant as no more ops left?
		}
		ti += 1;
	}

	return l;

}//duc_parse_expression*/

void duc_parse_while(ducx *d, token *tok){

	duc_get_next_token(tok);

	//TODO(GI):
	//later more complex conditions can go into while
	//for now just number as var or literals
	if(tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("For now, while() should always get a variable");
		exit(1);
	}

	loc *l = duc_parse_next_loc(d);

	l->type = DUC_OP_WHILE;

	if(tok->type == DUC_TOK_VAR_NAME ){
		var *v = duc_get_var(d, tok);
		l->operand1 = v;
	}

	d->csp[d->csp_counter] = l;
	d->csp_counter += 1;

	char *buffer = tok->start_next;
	while(*buffer && tec_char_is_white_space(*buffer)){
		buffer += 1;
	}
	if(*buffer != ')'){
		duc_print_error("condition of while must have closing bracket");
		exit(1);
	}
	buffer += 1;
	while(*buffer && tec_char_is_white_space(*buffer)){
		buffer += 1;
	}
	if(*buffer != '{'){
		duc_print_error("after while must come open curly brace {");
		exit(1);
	}
	tok->start_next = buffer + 1;

}//duc_parse_while*/

void duc_parse_print_string(ducx *d, token *tok){

	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_STRING && tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("print_string() should get a string (for now)");
		exit(1);
	}

	loc *l = duc_parse_next_loc(d);
	l->type = DUC_OP_FUNC_3;
	l->operator = &duc_exec_print_string;

	if(tok->type == DUC_TOK_VAR_NAME ){
		var *v = duc_get_var(d, tok);
		l->operand1 = v;
	}

	if(tok->type == DUC_TOK_STRING ){
		l->operand2 = tok->start;
		l->operand3 = tok->len;
	}

	//TODO(GI):
	//for now, we assume print_string ends as it should
	tok->start_next += 2;	//skip over );

}//duc_parse_print_string*/

void duc_parse_print_int(ducx *d, token *tok){

	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_NUMBER && tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("print_int() should get an int");
		exit(1);
	}

	loc *l = duc_parse_next_loc(d);
	l->type = DUC_OP_FUNC_3;
	l->operator = &duc_exec_print_int;

	if(tok->type == DUC_TOK_VAR_NAME ){
		//TODO(GI):
		// check we have an int and not something else
		var *v = duc_get_var(d, tok);
		l->operand1 = v;
	}

	if(tok->type == DUC_TOK_NUMBER ){
		l->operand2 = tok->start;
		l->operand3 = tok->len;
	}

	//TODO(GI):
	//for now, we assume print_int ends as it should
	tok->start_next += 2;	//skip over );

	return;

}//duc_parse_print_int*/

void duc_parse_exit(ducx *d, token *tok){

	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_NUMBER && tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("exit() should get an int");
		exit(1);
	}

	loc *l = duc_parse_next_loc(d);
	l->type = DUC_OP_FUNC_3;
	l->operator = &duc_exec_exit;

	if(tok->type == DUC_TOK_VAR_NAME ){
		//TODO(GI):
		// check we have an int and not something else
		var *v = duc_get_var(d, tok);
		l->operand1 = v;
	}

	if(tok->type == DUC_TOK_NUMBER ){
		l->operand2 = tec_string_to_int(tok->start);

	}

	//TODO(GI):
	//for now, we assume print_int ends as it should
	tok->start_next += 2;	//skip over );

	return;

}//duc_parse_exit*/

void duc_parse_print_error(ducx *d, token *tok){

	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_STRING && tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("print_error() takes a string");
		exit(1);
	}

	loc *l = duc_parse_next_loc(d);
	l->type = DUC_OP_FUNC_3;
	l->operator = &duc_exec_print_error;

	if(tok->type == DUC_TOK_VAR_NAME ){
		var *v = duc_get_var(d, tok);
		l->operand1 = v;
	}

	if(tok->type == DUC_TOK_STRING ){
		l->operand2 = tok->start;
		l->operand3 = tok->len;
	}

	//TODO(GI):
	//for now, we assume print_string ends as it should
	tok->start_next += 2;	//skip over );

}//duc_parse_print_error*/

void duc_run_bytecode(ducx *d){

	void (*func_0)(var *ret);
	void (*func_v)(var *v, var *ret);

	loc *l = &(d->block[0]);
	loc *lo = NULL;
	loc *lc = NULL;

	while(l->type){

		switch(l->type){
		case DUC_OP_INT_ASSIGN_L:
			lo = (loc *) l->operand2;
			l->ret.value.i = lo->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ASSIGN_V:
			l->ret.value.i = l->operand2->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ASSIGN_C:
			l->ret.value.i = (int)l->operand2;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_FUNC_0:
			//these functions take zero arguments
			//not including space to store return value
			func_0 = l->operator;
			func_0( &(l->ret) );
			break;
		case DUC_OP_FUNC_3:
			func_v = l->operator;
			func_v( &(l->operand1), &(l->ret) );
			break;
		case DUC_OP_FUNC_11:
			//not yet implemented
			break;
		case DUC_OP_INT_ADD_LL:
			lo = (loc *) l->operand2;
			lc = (loc *) l->operand3;
			l->ret.value.i = lo->ret.value.i + lc->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_LV:
			lo = (loc *) l->operand2;
			l->ret.value.i = lo->ret.value.i + l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_LC:
			lo = (loc *) l->operand2;
			l->ret.value.i = lo->ret.value.i + (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_VL:
			lo = (loc *) l->operand3;
			l->ret.value.i = l->operand2->value.i + lo->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_VV:
			l->ret.value.i = l->operand2->value.i + l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_VC:
			l->ret.value.i = l->operand2->value.i + (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_CL:
			lo = (loc *) l->operand3;
			l->ret.value.i = (int)l->operand2 + lo->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_CV:
			l->ret.value.i = (int)l->operand2 + l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_CC:
			l->ret.value.i = (int)l->operand2 + (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_LL:
			lo = (loc *) l->operand2;
			lc = (loc *) l->operand3;
			l->ret.value.i = lo->ret.value.i - lc->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_LV:
			lo = (loc *) l->operand2;
			l->ret.value.i = lo->ret.value.i - l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_LC:
			lo = (loc *) l->operand2;
			l->ret.value.i = lo->ret.value.i - (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_VL:
			lo = (loc *) l->operand3;
			l->ret.value.i = l->operand2->value.i - lo->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_VV:
			l->ret.value.i = l->operand2->value.i - l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_VC:
			l->ret.value.i = l->operand2->value.i - (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_CL:
			lo = (loc *) l->operand3;
			l->ret.value.i = (int)l->operand2 - lo->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_CV:
			l->ret.value.i = (int)l->operand2 - l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_CC:
			l->ret.value.i = (int)l->operand2 - (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_LL:
			lo = (loc *) l->operand2;
			lc = (loc *) l->operand3;
			l->ret.value.i = lo->ret.value.i * lc->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_LV:
			lo = (loc *) l->operand2;
			l->ret.value.i = lo->ret.value.i * l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_LC:
			lo = (loc *) l->operand2;
			l->ret.value.i = lo->ret.value.i * (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_VL:
			lo = (loc *) l->operand3;
			l->ret.value.i = l->operand2->value.i * lo->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_VV:
			l->ret.value.i = l->operand2->value.i * l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_VC:
			l->ret.value.i = l->operand2->value.i * (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_CL:
			lo = (loc *) l->operand3;
			l->ret.value.i = (int)l->operand2 * lo->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_CV:
			l->ret.value.i = (int)l->operand2 * l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_CC:
			l->ret.value.i = (int)l->operand2 * (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_LL:
			lo = (loc *) l->operand2;
			lc = (loc *) l->operand3;
			l->ret.value.i = lo->ret.value.i / lc->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_LV:
			lo = (loc *) l->operand2;
			l->ret.value.i = lo->ret.value.i / l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_LC:
			lo = (loc *) l->operand2;
			l->ret.value.i = lo->ret.value.i / (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_VL:
			lo = (loc *) l->operand3;
			l->ret.value.i = l->operand2->value.i / lo->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_VV:
			l->ret.value.i = l->operand2->value.i / l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_VC:
			l->ret.value.i = l->operand2->value.i / (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_CL:
			lo = (loc *) l->operand3;
			l->ret.value.i = (int)l->operand2 / lo->ret.value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_CV:
			l->ret.value.i = (int)l->operand2 / l->operand3->value.i;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_CC:
			l->ret.value.i = (int)l->operand2 / (int)l->operand3;
			if(l->operand1){
				l->operand1->value.i = l->ret.value.i;
			}
			break;
		}

		if(l->type == DUC_OP_JUMP){
			l = (loc *) l->operand1;
		}else{
			if(l->type == DUC_OP_WHILE){
				if(l->operand1->value.i){
					l += 1;
				}else{
					l = (loc *) l->operand2;
				}
			}else{
				//TODO(GI):
				//more complex logic will be needed here
				//once we use loc_long
				l += 1;
			}
		}
	}

}//duc_run_bytecode*/

void duc_exec_print_string(var **w, var *ret){

	var *v = *w;

	if(v){
		write(1, v->value.str, v->len);
	}else{
		w += 1;
		char *str = (char *) *w;
		w += 1;
		write(1, str, (int) *w);
	}

	ret->type = DUC_VAR_INT;
	ret->value.i = 1;

}//duc_exec_print_string*/

void duc_exec_print_int(var **w, var *ret){

	var *v = *w;

	if(v){
		char tmp[13];
		int n = tec_string_from_int( v->value.i, tmp, 12);
		tmp[n] = 10;
		n += 1;
		tmp[n] = 0;
		write(1, tmp, n);
	}else{
		w += 1;
		char *str = (char *) *w;
		w += 1;
		write(1, str, (int) *w);
		putchar(10);
	}

	ret->type = DUC_VAR_INT;
	ret->value.i = 1;

}//duc_exec_print_int*/

void duc_exec_exit(var **w, var *ret){

	var *v = *w;

	if(v){
		exit(v->value.i);
	}else{
		w += 1;
		exit((int)*w);
	}

}//duc_exec_exit*/

void duc_exec_print_error(var **w, var *ret){
//TODO(GI):
//clean this up a bit

	var *v = *w;

	char tmp[7] = "x[1;31m";
	tmp[0] = 27;
	write(2, tmp, 7);

	if(v){
		write(2, v->value.str, v->len);
	}else{
		w += 1;
		char *str = *w;
		w += 1;
		write(2, str, (int) *w);
	}

	char tmp2[5] = "x[0m";
	tmp2[0] = 27;
	write(2, tmp2, 5);

	ret->type = DUC_VAR_INT;
	ret->value.i = 1;

}//duc_exec_print_error*/

token* duc_get_next_token(token *tok){

	char *buffer = tok->start_next;
	int len = 0;

	while(*buffer && tec_char_is_white_space(*buffer)){
		buffer += 1;
	}

	if( !(*buffer) ){
		tok->type = DUC_TOK_EOF;
		return tok;
	}

	tok->start = buffer;

	if(*buffer == '*'){
		//TODO(GI):
		//in future we may need to check here whether
		//we are dealing with a multiplication
		//or a dereferenced pointer
		//for now, * is always muliplication
		tok->type = DUC_TOK_MATH_OP;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '/'){
		//TODO(GI):
		//in future we may need to check here whether
		//we are dealing with a division
		//or start of comment
		//for now, / is always division
		tok->type = DUC_TOK_MATH_OP;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '+'){
		//TODO(GI):
		//in future we may need to check here whether
		//we are dealing with an addition
		//or a positive number starting with +
		//for now, + is always addition
		tok->type = DUC_TOK_MATH_OP;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '-'){
		//TODO(GI):
		//in future we may need to check here whether
		//we are dealing with a subtraction
		//or a negative number
		//for now, - is always subtraction
		tok->type = DUC_TOK_MATH_OP;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '='){
		//TODO(GI):
		//for now, we ignore ==
		tok->type = DUC_TOK_EQUAL;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == ';'){
		tok->type = DUC_TOK_DELIMETER;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '('){
		tok->type = DUC_TOK_OPEN_BRACKET;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == ')'){
		tok->type = DUC_TOK_CLOSED_BRACKET;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '}'){
		tok->type = DUC_TOK_CLOSING_BLOCK;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '\"'){
		// tokenizer finds a string
		char prev = *buffer;
		buffer += 1;
		len += 1;
		while(*buffer && *buffer != '\"'){
			prev = *buffer;
			buffer += 1;
			len += 1;
			if(*buffer && *buffer == '\"' && prev == '\\'){
				buffer += 1;
				len += 1;
			}
		}

		//sanity checks for the strings
		if(!(*buffer)){
			duc_print_error("Unexpected end of file");
			exit(1);
		}
		if(len > DUC_MAX_STRING_SIZE){
			duc_print_error("String is too long!");
			exit(1);
		}

		*buffer = 0;
		tok->start += 1;
		duc_string_unescape_double_quotes_and_others(tok->start);
		buffer += 1;
		tok->len = tec_string_length(tok->start);

		//consider string not ending!!
//		tok->len = len;
		tok->type = DUC_TOK_STRING;
		tok->start_next = buffer;
		return tok;

	}

	if(*buffer == '$'){
		// tokenizer finds a variable name
		buffer += 1;
		len += 1;
		while(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') || (*buffer >= '0' && *buffer <= '9') || (*buffer == '_') ) ){
			//NOTE(GI):
			//our var can contain numerals 0 -9, just not begin with one
			len += 1;
			buffer += 1;
		}
		tok->len = len;
		tok->type = DUC_TOK_VAR_NAME;
		tok->start_next = buffer;
		return tok;
	}

	if( *buffer >= '0' && *buffer <= '9' ){
		// tokenizer finds a number
		buffer += 1;
		len += 1;
		while( *buffer && *buffer >= '0' && *buffer <= '9' ){
			buffer += 1;
			len += 1;
		}

		tok->len = len;
		tok->type = DUC_TOK_NUMBER;
		tok->start_next = buffer;
		return tok;
	}

	if(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') ) ){
		// tokenizer finds either keyword or function name

		while(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') || (*buffer >= '0' && *buffer <= '9') || (*buffer == '_') ) ){
			//NOTE(GI):
			//our function name can contain numerals 0 -9 and underscore, just not begin with one
			len += 1;
			buffer += 1;
		}

		tok->len = len;

		while( tec_char_is_white_space(*buffer) ){
			buffer += 1;
		}

		if( *buffer == '(' ){
			if(duc_check_keyword(tok)){
				tok->type = DUC_TOK_KEYWORD;
			}else{
				tok->type = DUC_TOK_FUNC_NAME;
			}
			tok->start_next = buffer + 1;
			return tok;
		}

		if( duc_check_datatype(tok) ){
			tok->type = DUC_TOK_DATATYPE;
			return tok;
		}

		tok->type = DUC_TOK_KEYWORD;
		tok->start_next = buffer;
		return tok;

	}

	return NULL;

}//duc_get_next_token*/

char* duc_string_unescape_double_quotes_and_others(char *str){

	if(!str)
		return str;

	char *original = str;
	while(*str){
		if(*str == '\\' && str[1] == '\"'){
			tec_string_shift(str);
		}
		if(*str == '\\' && str[1] == 'n'){
			//new line
			tec_string_shift(str);
			*str = 10;
		}
		if(*str == '\\' && str[1] == 't'){
			//tab
			tec_string_shift(str);
			*str = 9;
		}
		if(*str == '\\' && str[1] == 'r'){
			//carriage return
			tec_string_shift(str);
			*str = 13;
		}
		str += 1;
	}

	return original;

}//duc_string_unescape_double_quotes_and_others*/

int duc_check_datatype(token *tok){

	if(!tok)
		return 0;

	if( tec_buf_begins(tok->start, "int") && tok->len == 3 ){
		return 1;
	}
	if( tec_buf_begins(tok->start, "char") && tok->len == 4 ){
		duc_print_error("char not currently supported\n");
		exit(1);
	}
	if( tec_buf_begins(tok->start, "float") && tok->len == 5 ){
		duc_print_error("float not currently supported\n");
		exit(1);
	}

	return 0;

}//duc_check_datatype*/

int duc_check_keyword(token *tok){

	if(!tok)
		return 0;

	if( tec_buf_begins(tok->start, "while") && tok->len == 5 ){
		return 1;
	}
	if( tec_buf_begins(tok->start, "if") && tok->len == 2 ){
		duc_print_error("if not currently supported\n");
		exit(1);
	}
	if( tec_buf_begins(tok->start, "else") && tok->len == 4 ){
		duc_print_error("else not currently supported\n");
		exit(1);
	}

	return 0;

}//duc_check_keyword*/

void duc_create_variable(ducx *d, token *tok){

//	char *str_type = tok->start;
	tok->start_next = tok->start + tok->len;
	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("Invalid declaration of variable");
		exit(1);
	}

	if(tok->len > 31){
		duc_print_error("Max 31 characters for variable names\n");
		exit(1);
	}

	int hash = (tok->start[1] + tok->start[tok->len-1])%DUC_MAX_NUMBER_VARS;
	int loop = 0;	//this variable prevents infinite loop

	while( *(d->v_list_names[hash]) && loop < DUC_MAX_NUMBER_VARS ){
		if(tec_buf_begins(tok->start, d->v_list_names[hash]) ){
			duc_print_error("Variable name already used\n");
			exit(1);
		}
		hash += 1;
		if(hash == DUC_MAX_NUMBER_VARS){
			hash = 0;
		}
		loop += 1;
	}

	if(loop >= DUC_MAX_NUMBER_VARS){
		duc_print_error("Too many variables\n");
		exit(1);
		return;
	}

	char *name = d->v_list_names[hash];
	int len = tok->len;
	char *tmp = tok->start;
	while(len){
		*name = *tmp;
		name += 1;
		len -= 1;
		tmp += 1;
	}
	//null character already at end

	var *v = &(d->v_list[hash]);
//NOTE(GI):
//right now, all variables are int anyway
//	if( tec_buf_begins(str_type, "int") ){
//		v->type = DUC_VAR_INT;
//	}

	//check for initialization
	char *eq = tok->start + tok->len;
	while(*eq && tec_char_is_white_space(*eq)){
		eq += 1;
	}
	if(*eq != '='){
		duc_print_error("Please initialize your variables\n");
		exit(1);
		return;
	}
	tok->start_next = eq + 1;

	loc *l = duc_parse_expression(d, tok, 0);
	if(l){
			l->operand1 = v;
		}

}//duc_create_variable*/

var* duc_get_var(ducx *d, token *tok){

	int hash = (tok->start[1] + tok->start[tok->len-1])%DUC_MAX_NUMBER_VARS;

	int loop = 0;	//this variable prevents infinite loop
	while(!tec_buf_begins(tok->start, d->v_list_names[hash]) && loop < DUC_MAX_NUMBER_VARS ){
		hash += 1;
		if(hash == DUC_MAX_NUMBER_VARS){
			hash = 0;
		}
		loop += 1;
	}

	if(loop >= DUC_MAX_NUMBER_VARS){
		duc_print_error("Variable not found\n");
		exit(1);
		return NULL;
	}

	if( *(d->v_list_names[hash]) && tec_buf_begins(tok->start, d->v_list_names[hash]) ){
		return &(d->v_list[hash]);
	}else{
		duc_print_error("Variable not found\n");
		exit(1);
		return NULL;
	}

}//duc_get_var*/

void duc_debug_display_token(token *tok){

	if(!tok)
		return;

	char *tmp = tok->start;
	int len = tok->len;
	printf("token is *%s", TC_RED);
	while(len){
		putchar(*tmp);
		len -= 1;
		tmp += 1;
	}
	printf("%s*\n", TC_NRM);

}//duc_debug_display_token*/

void duc_debug_print_block(ducx *d){

	int i = 0;
	loc *l = &(d->block[0]);

	while(i < 10){
		printf("loc:\n");

		switch(l->type){
		case DUC_OP_NOP:
			printf("no operation\n");
			break;
		case DUC_OP_FUNC_0:
			printf("function 0 args\n");
			break;
		case DUC_OP_FUNC_3:
			printf("function up to 3 args\n");
			break;
		case DUC_OP_FUNC_11:
			printf("function 11 args\n");
			break;
		case DUC_OP_WHILE:
			printf("while loop\n");
			break;
		case DUC_OP_INT_ADD_VC:
		case DUC_OP_INT_ADD_CV:
		case DUC_OP_INT_ADD_VV:
			printf("integer addition\n");
			break;
		case DUC_OP_INT_SUB_VV:
		case DUC_OP_INT_SUB_VC:
		case DUC_OP_INT_SUB_CV:
			printf("integer subtraction\n");
			break;
		case DUC_OP_INT_MUL_VC:
		case DUC_OP_INT_MUL_CV:
		case DUC_OP_INT_MUL_VV:
			printf("integer multiplication\n");
			break;
		case DUC_OP_INT_DIV_VV:
		case DUC_OP_INT_DIV_VC:
		case DUC_OP_INT_DIV_CV:
			printf("integer division\n");
			break;
		}

		printf("\n***************\n");
		l += 1;
		i += 1;
	}

}//duc_debug_print_block*/

void duc_debug_display_terms(term *t){

	int i = 0;

	while(t[i].op){
		i += 1;
	}
	printf("%d term(s) detected\n", i);

	i = 0;
	while(t[i].op){
		if(t[i].op == ' '){
			printf("*processed*\n");
		}else{
			printf("operation here %c\n", t[i].op);
		}
		if(!t[i].v){
			printf("int is %d\n", t[i].value.i);
		}
		i += 1;
	}

}//duc_debug_display_terms*/
