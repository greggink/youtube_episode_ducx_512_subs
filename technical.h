#ifndef TECHNICAL_H
#define TECHNICAL_H

#ifndef NULL
#define NULL \0
#endif

#ifndef MAX_PATH
#define MAX_PATH 260
// MAX_PATH including NULL character
#define MAX_PATH_INC MAX_PATH + 1
#endif

#define TEC_MOST_SIG_BIT 128
#define TEC_2ND_MOST_SIG_BIT 64

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>

/*tec_file_exists:
	returns 1 if file exists
	returns 0 in all other cases ( doesn't exist or error )
*/
int			tec_file_exists(char *path);

/*tec_file_get_size:
	returns the size of the file in bytes
	returns -1 in case of error e.g. file does not exist
*/
int64_t		tec_file_get_size(char *path);

/*tec_file_get_contents:
	this will return the raw contents of a file in a NULL terminated buffer
	returns NULL in case of error (file does not exist, no read permission, etc.)
	free the returned buffer
*/
char*		tec_file_get_contents(char *path);

/*tec_char_is_white_space:
	returns 1 if c is a white space character (e.g. space)
	returns 0 otherwise
	assumes 7 bit ascii character
	there are more white space characters within unicode
	they are not so commonly used and could not all be considered in just an 8 bit character
*/
int			tec_char_is_white_space(char c);

/*tec_buf_begins:
	returns 1 if the buffer begins with string str
	returns 0 in all other cases, including errors and str being longer than buffer
*/
int			tec_buf_begins(char *buffer, char *str);

/*tec_string_shift:
	removes an ascii char or unicode codepoint at front of string
	assumes a valid utf8 string
*/
void		tec_string_shift(char *str);

/*tec_string_length:
	returns the length of a string in bytes
	check tec_string_utf8_length to know the number of codepoints
	unlike strlen, this function does not segfault when you give it a NULL pointer
	instead it returns zero because, well, you gave it an empty string ;-)
*/
int			tec_string_length(char *str);

/*tec_string_copy:
	safer alternative to strcpy or even strncpy
	int size is the size of the destination
	these functions guarantee that at most size - 1 bytes will be written to destination plus a NULL character
	this prevents buffer overflows
	this guarantees you get a NULL terminated string in destination (unlike strncpy)
	this function will not cut off the copying in the middle of a UTF8 codepoint when out of space
	returns 1 if all was copied right
	returns 2 if source could not be fully copied
	returns 0 in case of error

	these functions assume char *source is a correctly formatted UTF8 string
*/
int			tec_string_copy(char *destination, char *source, int size);

/*tec_string_to_int:
	converts a string to an integer
	string may not contain anything in front of number except '-' or '+'
	does not safeguard against overflow
*/
int			tec_string_to_int(char *s);

/*tec_string_from_int:
	takes an integer and converts it to a string
	writes the result into char* buffer
	returns the number of bytes written (including NULL character)
	returns -1 upon failure
*/
int			tec_string_from_int(int32_t i, char *buffer, int32_t buffer_size);

#endif