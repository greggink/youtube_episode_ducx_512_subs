#include "technical.h"

int tec_file_exists(char *path){

	if(!path)
		return 0;

	if(access(path, F_OK) != -1){
		return 1;
	}else{
		return 0;
	}

}//tec_file_exists*/

int64_t tec_file_get_size(char *path){

	if(!path)
		return -1;

#ifdef __linux
	struct stat st;
	int error = stat(path, &st);
	if(!error){
		return (int64_t) st.st_size;
	}else{
		return -1;
	}
#endif

#ifdef __WIN32
	BOOL test = FALSE;
	int64_t size;
	HANDLE fp = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	test = GetFileSizeEx(fp, (PLARGE_INTEGER) &size);
	CloseHandle(fp);
	if(test){
		return size;
	}
#endif

	return -1;

}//tec_file_get_size*/

char* tec_file_get_contents(char *path){

	if(!path)
		return NULL;

	int64_t size = tec_file_get_size(path);
	if(size == -1)
		return NULL;

	size += 1;
	FILE *fp = fopen(path, "rb");
	if(!fp){
		return NULL;
	}
	char *buffer = (char *) malloc(sizeof(char) * size);
	fread(buffer, sizeof(char), size, fp);
	fclose(fp);
	buffer[size-1] = 0;
	return buffer;

}//tec_file_get_contents*/

int tec_char_is_white_space(char c){

	if(c == 32 || c == 9 || c == 10 || c == 11 || c == 12 || c == 13)
		return 1;
	return 0;

}//tec_char_is_white_space*/

int tec_buf_begins(char *buffer, char *str){

	if(!buffer)
		return 0;
	if(!str)
		return 0;

	while(*str && *buffer == *str){
		str += 1;
		buffer += 1;
	}

	if(*str){
		return 0;
	}

	return 1;

}//tec_buf_begins*/

void tec_string_shift(char *str){

	if(!str)
		return;

	int len = tec_string_length(str);
	int i = 1;
	int j = 1;

	if( (str[i-j] & TEC_MOST_SIG_BIT) && (str[i-j] & TEC_2ND_MOST_SIG_BIT) ){
		j += 1;
		i += 1;
		while( (str[i] & TEC_MOST_SIG_BIT) && !(str[i] & TEC_2ND_MOST_SIG_BIT) ){
			j += 1;
			i += 1;
		}
	}

	while(i < len){
		str[ i - j ] = str[i];
		i += 1;
	}
	str[i - j] = '\0';

}//tec_string_shift*/

int tec_string_length(char *str){

	if(!str)
		return 0;
	if(!*str)
		return 0;

	int len = 0;

#if __x86_64__

	int64_t *str_i = (int64_t *) str;
	int64_t addr = (int64_t) str_i;

	// 64 bit computer
	// ensure str is on 8-byte boundary before using speed-up trick
	while( addr&0x0000000000000007 && *str ){
		len += 1;
		str += 1;
		addr = (int64_t) str;
	}

	if(!*str){
		return len;
	}

	// check for NULL characters, 8 bytes at a time
	// https://graphics.stanford.edu/~seander/bithacks.html#ZeroInWord
	str_i = (int64_t *) str;
	while( !( ( *str_i - 0x0101010101010101 ) & ~(*str_i) & 0x8080808080808080 ) ){
		len += 8;
		str_i += 1;
	}

	str = (char *) str_i;
	while(*str){
		len += 1;
		str += 1;
	}

#else

	int32_t *str32_i = (int32_t *) str;
	int32_t addr32 = (int32_t) str32_i;

	// 32 bit computer
	// ensure str is on 4-byte boundary before using speed-up trick
	while( addr32&0x00000003 && *str ){
		len += 1;
		str += 1;
		addr32 = (int32_t) str;
	}

	if(!*str){
		return len;
	}

	// check for NULL characters, 4 bytes at a time
	// https://graphics.stanford.edu/~seander/bithacks.html#ZeroInWord
	str32_i = (int32_t *) str;
	while( !( ( *str32_i - 0x01010101 ) & ~(*str32_i) & 0x80808080 ) ){
		len += 4;
		str32_i += 1;
	}

	str = (char *) str32_i;
	while(*str){
		len += 1;
		str += 1;
	}

#endif

	return len;

}//tec_string_length*/

int tec_string_copy(char *destination, char *source, int size){

	if(!destination)
		return 0;
	if(!source){
		*destination = 0;
		return 1;
	}
	if(size <= 0)
		return 0;

	size -= 1;

	int i = 0;
	while(*source && i < size){
		destination[i] = *source;
		source += 1;
		i += 1;
	}

	// we don't want to cut off the copying in the middle of a UTF8 codepoint
	// firstly check whether the next byte of source is either not present or the start of a codepoint
	if(*source && (*source & TEC_MOST_SIG_BIT) && !(*source & TEC_2ND_MOST_SIG_BIT) ){
		i -= 1;
		// this while loop goes back while we have the 2nd, 3rd or 4th byte of a UTF8 codepoint
		while( (destination[i] & TEC_MOST_SIG_BIT) && !(destination[i] & TEC_2ND_MOST_SIG_BIT) ){
			i -= 1;
		}
		// this goes back from the head of a UTF8 codepoint
		if( (destination[i] & TEC_MOST_SIG_BIT) && (destination[i] & TEC_2ND_MOST_SIG_BIT) ){
			destination[i] = 0;
		}else{
			// should never happen, this would be invalid UTF8
			destination[i] = 0;
			return 0;
		}
	}

	destination[i] = '\0';

	if(*source){
		return 2;
	}else{
		return 1;
	}

}//tec_string_copy*/

int tec_string_to_int(char *s){

	if(!s)
		return 0;

	int sign = 1;
	int result = 0;

	while(tec_char_is_white_space(*s)){
		s += 1;
	}
	if(*s == '-'){
		sign = -1;
		s += 1;
	}else{
		if(*s == '+'){
			s += 1;
		}
	}

	while(*s){
		if(*s > '9' || *s < '0'){
			return result * sign;
		}
		result *= 10;
		result += *s - '0';
		s += 1;
	}

	return result * sign;

}//tec_string_to_int*/

int tec_string_from_int(int32_t i, char *result, int32_t buffer_size){

	if(!result)
		return -1;

	int tmp = i;
	int len = 1;
	int j = 0;
	int negative = 0;

	if(i < 0){
		negative = 1;
		len += 1;
		tmp *= -1;
	}

	while(tmp > 9){
		tmp /= 10;
		len += 1;
	}

	if(len + 1 > buffer_size){
		result[0] = '\0';
		return -1;
	}

	result[len] = '\0';
	j = len;
	if(negative){
		result[0] = '-';
		i *= -1;
	}
	while(j - negative){
		j -= 1;
		result[j] = i%10 + '0';
		i /= 10;
	}

	return len + 1;	//+1 accounts for NULL character

}//tec_string_from_int*/
